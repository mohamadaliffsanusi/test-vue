import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './views/HomeView.vue'
import Category from './views/CategoryView.vue'
import Search from './views/SearchView.vue'

Vue.use(VueRouter);

const routes = [
  {path: '/', component: Home },
  {path: '/category/:id', component: Category,props: true},
  {path: '/search', component: Search,props: true}
  // Add more routes as needed
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
  })

export default router;