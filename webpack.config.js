'use strict'

const path = require("path");
const { VueLoaderPlugin } = require('vue-loader');
const dataObj = require("./data/data.json");

module.exports = {
	entry: [
		'./src/app.js'
	],
	output: {
		path: path.resolve(__dirname, './dist'),
		publicPath: '/dist/',
		filename: 'bundle.js'
    },
	module: {
		rules: [{
			test: /\.vue$/,
			use: 'vue-loader'
		},
		{
			test: /\.scss$/,
			use: [
				'vue-style-loader',
				'css-loader',
				'sass-loader',
			]
		},
		// {
		// 	test: /\.js$/,
		// 	exclude: /node_modules/,
		// 	use: {
		// 	  loader: 'babel-loader'
		// 	}
		//   },
		{
            test: /.css$/,  // Add this rule
            use: [
                'vue-style-loader',  // This could be 'style-loader' if you are not specifically using vue-style-loader features
                'css-loader'
            ]
        },
		{
			test: /\.(png|jpe?g|gif|svg)$/i,
			use: [
			  {
				loader: 'url-loader',
				options: {
				  limit: 8192, // Converts files up to 8kb to base64 strings
				  name: 'images/[name].[ext]', // Output path for images
				},
			  },
			],
		  },
		{
			test: /\.(ttf|eot|woff|woff2)$/,
			use: {
				loader: 'url-loader',
				options: {
					name: '[name].[ext]',
				},
			},
		  }],
		  
		  
	},
	plugins: [
		new VueLoaderPlugin()
	],
	resolve: {
		alias: {
			fonts : path.resolve(__dirname, 'src/assets/fonts')
		}
	},
	devServer: {
		contentBase: path.join(__dirname, 'public'),
		port: 9000,
		historyApiFallback: true,
		before: function(app, server, compiler) {
			app.get('/api/categories', function (req, res) {
				res.json(dataObj.categories);
			});

			app.get('/api/category/*', function (req, res) {
				res.json(dataObj.articles);
			});

			app.get('/api/author/*', function (req, res) {
				let author = {};
				const authorId = req.params['0'];

				for (let index = 0; index < dataObj.authors.length; index++) {
					if (dataObj.authors[index].id === authorId) {
						author = dataObj.authors[index];
						break;
					}
					
				}
				res.json(author);
			});

			app.get('/api/search/*', function (req, res) {
				res.json(dataObj.articles);
			});
		}
	}
}